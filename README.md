# JWT's README

To run: java -jar jwt-exemple-danilo-0.0.1-SNAPSHOT.jar
 
 
The idea is to focus only on the JWT implementation, so that there is only two fixed users, "admin" and "normalUser", both with the password "12@34".

There will be three endpoints: 
•	 POST auth: http://localhost:8080/auth; expecting the json: {"username":"admin","password":"12@34"}
•	 GET persons: http://localhost:8080/persons
•	 GET protected: http://localhost:8080/protected; this one, available only to the admin user
 
The JWT token is always available for only 1 minute, after that, it expires. 

To test you can follow these steps:
 
AUTH=`curl -H "Content-Type: application/json" -X POST -d '{"username":"admin","password":"12@34"}' http://localhost:8080/auth`; echo $AUTH;printf '\n'
 
curl -H "Authorization: $AUTH" http://localhost:8080/persons;printf '\n'
curl -H "Authorization: $AUTH" http://localhost:8080/protected;printf '\n'
 
AUTH=`curl -H "Content-Type: application/json" -X POST -d '{"username":"normalUser","password":"12@34"}' http://localhost:8080/auth`; echo $AUTH;printf '\n'
 
curl -H "Authorization: $AUTH" http://localhost:8080/persons;printf '\n'
curl -H "Authorization: $AUTH" http://localhost:8080/protected;printf '\n

