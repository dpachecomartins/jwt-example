package com.example.demo.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.security.JwtUser;
import com.example.demo.security.JwtUserFactory;


@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	if ("admin".equals(username)) {
            return JwtUserFactory.create("admin");    		
    	} else if ("normalUser".equals(username)) {
            return JwtUserFactory.create("normalUser");    		    	
        } else {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }
    }
}
