package com.example.demo.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.security.JwtAuthenticationRequest;
import com.example.demo.security.JwtTokenUtil;

@RestController
public class AuthenticationRestController {
		
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
	
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,Device device) throws Exception {
    	if (!authenticationRequest.getPassword().equals("12@34")) {
    		throw new Exception("Invalid password. It must me 12@34");
    	}
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    	final String token = jwtTokenUtil.generateToken(userDetails, device);
        return ResponseEntity.ok(token);
    }
}
