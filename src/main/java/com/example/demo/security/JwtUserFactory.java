package com.example.demo.security;

public final class JwtUserFactory {
	
    private JwtUserFactory() {
    }

    public static JwtUser create(String user) {
    	if ("admin".equals(user)) {
    		return new JwtUser(user, "danilo.pacheco-martins@siicanada.com", null);
    	} else if ("normalUser".equals(user)) {
    		return new JwtUser(user, "tl.huynhhuu@facilite.com", null);    		
    	}
    	return null;
    }
    

}
