package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtExempleDaniloApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtExempleDaniloApplication.class, args);
	}
}
